from celery import Task
from django.db import connection


class DBTask(Task):
    abstract = True

    def after_return(self, *args, **kwargs):
        if connection.connection is not None:
            connection.close()
