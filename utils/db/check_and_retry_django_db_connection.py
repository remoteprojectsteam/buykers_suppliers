import time

from celery.utils.log import get_task_logger
from django.db import close_old_connections, connection
from django.db.utils import OperationalError


def check_and_retry_django_db_connection(logger_name):
    logger = get_task_logger(logger_name)

    close_old_connections()

    db_conn = False
    while not db_conn:
        try:
            connection.ensure_connection()
            db_conn = True
        except OperationalError:
            logger.error('Database unavailable, waiting 1 second...')
            time.sleep(1)

    logger.error('Database available')
