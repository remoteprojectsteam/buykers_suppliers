from decimal import Decimal

from django.db import models


class Category(models.Model):
    """Category model"""

    STATUS_CHOICE_NEW = 'NEW'
    STATUS_CHOICE_PROGRESS = 'PROGRESS'
    STATUS_CHOICE_DONE = 'DONE'
    STATUS_CHOICE_ERROR = 'ERROR'

    STATUS_CHOICES = (
        (STATUS_CHOICE_NEW, 'Новый'),
        (STATUS_CHOICE_PROGRESS, 'В обработке'),
        (STATUS_CHOICE_DONE, 'Закончен'),
        (STATUS_CHOICE_ERROR, 'Ошибка')
    )

    status = models.CharField(verbose_name='Статус', max_length=255, choices=STATUS_CHOICES, default=STATUS_CHOICE_NEW)
    name = models.CharField(verbose_name='Наименование', max_length=255)
    link = models.TextField(verbose_name='Ссылка на категорию')

    gender = models.CharField(verbose_name='Пол', max_length=255, default='')
    in_stock = models.CharField(verbose_name='Наличие', max_length=255, blank=True, default='')
    margin = models.DecimalField(verbose_name='Наценка (%)', decimal_places=2, max_digits=12, default=Decimal(0))
    delivery = models.DecimalField(verbose_name='Доставка', decimal_places=2, max_digits=12, default=Decimal(0))
    euro_rate = models.DecimalField(verbose_name='Курс евро', decimal_places=2, max_digits=12, default=Decimal(80))

    created_at = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Дата обновления', auto_now=True)

    category_name = models.CharField(verbose_name='Название категории', max_length=255, default='')

    template_title = models.CharField(verbose_name='Шаблон Заголовка товаров', max_length=255, blank=True,
                                      null=True, default='{model_name} {category_name}'
                                                         ' - самые выгодные цены в магазине buykers.ru')
    template_meta_keywords = models.CharField(verbose_name='Шаблон META Keywords товаров', max_length=255,
                                              blank=True, null=True, default='{model_name}, {category_name}'

                                              )
    template_meta_description = models.TextField(verbose_name='Шаблон META Description товаров', blank=True, null=True,

                                                 default='Купить {model_name} {category_name} в '
                                                         'наличии, в москве, в россии, в '
                                                         'магазине buykers.ru '
                                                         ''
                                                         '✔ Доступная низкая цена на распродаже, '
                                                         '{category_name} для мотоциклиста, '
                                                         'бесплатная доставка ☎ +74953749470'
                                                 )

    template_name = models.CharField(verbose_name='Шаблон Наименования товаров', max_length=255, blank=True, null=True,
                                     default='{model_name} {category_name} {color}')

    bage_html = models.CharField(verbose_name='Наклейка', max_length=255,
                                              blank=True, null=True)

    keywords = models.CharField(verbose_name='Теги', max_length=255, help_text='Укажите через запятую',
                                 blank=True, null=True)

    class Meta:
        """Meta"""

        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return f'{self.name}'
