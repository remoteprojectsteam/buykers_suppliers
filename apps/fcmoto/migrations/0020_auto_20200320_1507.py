# Generated by Django 2.2.7 on 2020-03-20 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcmoto', '0019_auto_20200320_1504'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='meta_description',
            field=models.TextField(blank=True, null=True, verbose_name='META Description'),
        ),
        migrations.AddField(
            model_name='product',
            name='meta_keywords',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='META Keywords'),
        ),
        migrations.AddField(
            model_name='product',
            name='title',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок'),
        ),
    ]
