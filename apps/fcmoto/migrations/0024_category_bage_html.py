# Generated by Django 2.2.7 on 2020-04-12 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcmoto', '0023_auto_20200405_1509'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='bage_html',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Наклейка'),
        ),
    ]
