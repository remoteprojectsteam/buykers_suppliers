# Generated by Django 2.2.7 on 2020-11-15 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcmoto', '0024_category_bage_html'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='in_stock',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Наличие'),
        ),
        migrations.AddField(
            model_name='category',
            name='keywords',
            field=models.CharField(blank=True, help_text='Укажите через запятую', max_length=255, null=True, verbose_name='Теги'),
        ),
        migrations.AlterField(
            model_name='category',
            name='template_name',
            field=models.CharField(blank=True, default='{model_name} {category_name} {color}', max_length=255, null=True, verbose_name='Шаблон Наименования товаров'),
        ),
    ]
