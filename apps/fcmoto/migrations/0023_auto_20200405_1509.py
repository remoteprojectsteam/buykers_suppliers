# Generated by Django 2.2.7 on 2020-04-05 12:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcmoto', '0022_auto_20200405_1241'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='template_name',
            field=models.CharField(blank=True, default='{model_name} {category_name}', max_length=255, null=True, verbose_name='Шаблон Наименования товаров'),
        ),
        migrations.AlterField(
            model_name='category',
            name='template_meta_keywords',
            field=models.CharField(blank=True, default='{model_name}, {category_name}', max_length=255, null=True, verbose_name='Шаблон META Keywords товаров'),
        ),
    ]
