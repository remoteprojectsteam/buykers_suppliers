import logging
import re
import time
from decimal import Decimal, InvalidOperation

import cloudscraper
from bs4 import BeautifulSoup
from furl import furl

from ..models import Product


can_add_to_basket_var_pattern = re.compile("(?<=CanAddToBasket:)[^,]*")
all_script_pattern = re.compile("attributeValues\[\d+\] *\= *'\d+';[^}]+")
model_name_pattern = re.compile(r"([a-z-A-Z- -9]+)")


class ProductParser:
    """Product parser"""

    logger = logging.getLogger(__name__)

    def get_products(self, products_items):
        """Get products"""
        products = []
        scraper = cloudscraper.create_scraper(browser='chrome')
        try:
            for product_item in products_items:
                try:

                    new_item_data = self.get_product(scraper, product_item.get("link"))

                    new_item_data["status"] = Product.STATUS_CHOICE_DONE
                    products.append({**new_item_data, **product_item})
                except Exception as exp:
                    new_item_data = {"status": Product.STATUS_CHOICE_ERROR, "name": str(exp)}
                    products.append({**new_item_data, **product_item})
                time.sleep(1)
        except:
            pass
        return products

    def get_product(self, scraper, link) -> dict:
        """Get single product"""
        r = scraper.get(link)
        if r.status_code > 204: raise Exception('Error {} trying get this link'.format(r.status_code))
        soup = BeautifulSoup(r.text, "lxml")

        try:
            name = soup.select_one('.ICProductVariationArea [itemprop="name"]').text
        except:
            name = ""

        try:
            price = soup.select_one(".PriceArea .Price").text.replace("\n", "")
        except:
            price = ""
        try:
            price_cleaned = Decimal(price.replace("руб.", "").replace(" ", "").replace(",", "."))
        except InvalidOperation:
            price_cleaned = None

        try:
            manufacturer = soup.select_one('.ICProductVariationArea [itemprop="manufacturer"]').text
        except:
            manufacturer = ""

        try:
            color = soup.select_one(".ICVariationSelect .Headline.image .Bold.Value").text
        except:
            color = ""

        available_sizes = []
        disabled_sizes = []
        try:
            pre_def_value_id = None
            tag_group = soup.select(".ICAttributBar")
            if len(tag_group) > 1:
                pre_def_value_id = tag_group[0].select_one("img.selected").get("predefvalueid")
            elif len(tag_group) == 1:
                pre_def_value_id = "ALL"
            size_names = [i.get("value") for i in tag_group[-1].select("button")]
            script_tag = tag_group[-1].find_next("script").next
            if pre_def_value_id != "ALL":
                sizes_script_data = re.findall(
                    "attributeValues\[\d+\] *\= *'" + str(pre_def_value_id) + "';[^}]+", script_tag
                )
            else:
                sizes_script_data = all_script_pattern.findall(script_tag)
            for size_count in range(0, len(sizes_script_data)):
                size_data = sizes_script_data[size_count].replace(" ", "")
                size_name = size_names[size_count]
                try:
                    num = int(can_add_to_basket_var_pattern.search(size_data).group())
                    if num > 0:
                        available_sizes.append(size_name)
                    else:
                        disabled_sizes.append(size_name)
                except:
                    pass
        except Exception as e:
            pass

        try:
            front_picture = 'https://www.fc-moto.de' + soup.select_one("#ICImageMediumLarge").get("data-src")
        except:
            front_picture = ""

        try:
            back_picture = 'https://www.fc-moto.de' + soup.select_one("#ProductThumbBar").select("img")[1].get("src").replace("_s.jpg", "_ml.jpg")
        except:
            back_picture = ""

        try:
            des = soup.select_one('.description[itemprop="description"]')
            description_text = des.text.strip()
            description_html = str(des).strip()
        except:
            description_text = ""
            description_html = ""

        try:
            description_en = soup.select("div.description")[1]
            description_en_text = description_en.text.strip() if description_en else ""
            description_en_html = str(description_en).strip() if description_en else ""
        except:
            description_en_html = ""
            description_en_text = ""

        sizes = [{"available": False, "size": size} for size in disabled_sizes]
        sizes.extend([{"available": True, "size": size} for size in available_sizes])
        attributes = {"sizes": sizes, "color": color}

        name = name.replace("Büse", "Buse")
        name = name.replace("Fjäll Räven", "Fjall Raven")
        name = name.replace("Schöffel", "Schoffel")
        name = name.replace("ö", "o")
        name = name.replace("ä", "a")
        name = name.replace("ü", "u")
        name = name.replace("O’Neal", "Oneal")
        try:
            model_name = model_name_pattern.search(name).group(0).strip()
        except Exception:
            # self.logger.info('Model name error')
            model_name = name

        try:
            vendor_code = soup.select_one("#MicroDataMPN").get("content")
            print(vendor_code)
        except:
            vendor_code = ""

        product_item = {
            "name": name,
            "manufacturer": manufacturer,
            "name_url": furl(link.replace("?", "")).path.segments[-1],
            "price": price_cleaned,
            "front_picture": front_picture,
            "back_picture": back_picture,
            "description_text": description_text,
            "description_html": description_html,
            "description_en_text": description_en_text,
            "description_en_html": description_en_html,
            "model_name": model_name,
            "attributes": attributes,
            "vendor_code": vendor_code,
        }
        return product_item
