import cloudscraper
import pydash
from bs4 import BeautifulSoup
from furl import furl


class CategoryParser:
    """Category parser"""

    def __init__(self, category_link):
        """Init"""
        self.category_link = category_link
        self.soup = self.get_soup()

    def get_soup(self):
        """Get soup"""
        scraper = cloudscraper.create_scraper(browser='chrome')
        page = scraper.get(self.category_link, timeout=30)
        soup = BeautifulSoup(page.text, 'lxml')
        return soup

    def get_pages(self):
        """Get pages"""
        pages_list = []
        try:
            pages = self.soup.find('ul', class_='PagerSizeContainer').find_all('li')
            links = [pydash.get(page, 'a.href') for page in pages]
            if len(links) > 1:
                link = links[-2]
                last_page = furl(link).args.get('Page')
                last_page = int(last_page)
                base_page_url = 'https://www.fc-moto.de/epages/fcm.sf/ru_RU/'
                for page in range(1, last_page + 1):
                    page_link = furl(link).remove('Page').add({"Page": page}).url
                    pages_list.append(f'{base_page_url}{page_link}')
        except AttributeError:
            pages_list.append(self.category_link)
        return pages_list
