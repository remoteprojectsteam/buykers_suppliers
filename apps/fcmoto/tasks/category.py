from celery import shared_task
from django.core.exceptions import ObjectDoesNotExist
from requests.exceptions import ConnectionError

from .page import page_task
from ..models import Category, Page, Product
from ..parsers import CategoryParser


@shared_task(bind=True, max_retries=3)
def category_task(self, category_id):
    """Category task"""
    category = None

    try:
        category = Category.objects.get(id=category_id)

        # set status
        category.status = Category.STATUS_CHOICE_PROGRESS
        category.save()

        # parse category's pages
        category_parser = CategoryParser(category_link=category.link)
        pages = category_parser.get_pages()

        # set is_active to False
        Page.objects.filter(category=category).update(is_active=False)

        # update pages
        for page in pages:
            page, _ = Page.objects.update_or_create(page_url=page, category=category,
                                                    defaults={
                                                        'status': Page.STATUS_CHOICE_NEW,
                                                        'is_active': True,
                                                    })
            page_task.delay(page_id=page.id)

        # set status
        category.status = Category.STATUS_CHOICE_DONE
        category.save()
    except ObjectDoesNotExist:
        pass
    except ConnectionError:
        if category and hasattr(category, 'save'):
            # set status
            category.status = Category.STATUS_CHOICE_ERROR
            category.save()

            # retry task
            self.retry(countdown=10)


@shared_task(bind=True, max_retries=3)
def category_meta_update_task(self, category_id):
    """Category task"""
    category = None

    try:
        category = Category.objects.get(id=category_id)

        for product_item in Product.objects.filter(category=category):
            product = Product.objects.get(pk=product_item.pk)
            model_name = '' if product_item.model_name is None else product_item.model_name
            category_name = product_item.category.category_name
            color_value = product.attributes.get('color', '')
            color_with = 'Цвет ({0})'.format(color_value) if color_value else ''

            product.name = ('{model_name} {category_name} {color}'.format(
                model_name=model_name,
                category_name=category_name,
                color=color_value,
                color_with=color_with
            ) if not product.category.template_name else str(product.category.template_name).format(
                model_name=model_name,
                category_name=category_name,
                color=color_value,
                color_with=color_with
            )).strip()

            # title
            product.title = '{model_name} {category_name} - самые выгодные цены в магазине buykers.ru'.format(
                model_name=model_name,
                category_name=category_name
            ) if not product_item.category.template_title else str(product_item.category.template_title).format(
                model_name=model_name,
                category_name=category_name
            )

            # meta_keywords
            product.meta_keywords = '{model_name}, {category_name}'.format(
                model_name=model_name,
                category_name=category_name
            ) if not product_item.category.template_meta_keywords else str(
                product_item.category.template_meta_keywords).format(
                model_name=model_name,
                category_name=category_name
            )

            # meta_description
            meta_description_default = 'Купить {model_name} {category_name} в ' \
                                       'наличии, в москве, в россии, в ' \
                                       'магазине buykers.ru ' \
                                       '' \
                                       '✔ Доступная низкая цена на распродаже, ' \
                                       '{category_name} для мотоциклиста, ' \
                                       'бесплатная доставка ☎ +74953749470'

            product.meta_description = str(meta_description_default).format(
                model_name=model_name,
                category_name=category_name
            ) if not product_item.category.template_meta_description else str(
                product_item.category.template_meta_description).format(
                model_name=model_name,
                category_name=category_name
            )
            product.save()

    except ObjectDoesNotExist:
        pass
