import re
from datetime import timedelta

from celery import shared_task
from celery.utils.log import get_task_logger
from django.db import IntegrityError
from django.utils.datetime_safe import datetime
from transliterate import detect_language, translit

from ..models import Product
from ..parsers import ProductParser


@shared_task(bind=True, max_retries=1, time_limit=60 * 10, soft_time_limit=60 * 10)
def product_task(self, product_ids):
    """Product task"""
    try:
        products = Product.objects.filter(id__in=product_ids)
        products.update(status=Product.STATUS_CHOICE_PROGRESS)
        products_items = [
            {"id": product.id, "link": product.link, "product_name_url_color": product.name_url_color}
            for product in products
        ]

        product_parser = ProductParser()
        items = product_parser.get_products(products_items=products_items)
        # items: [{'id':1 , 'price':23412, ..}, {'id':2 , 'price':2342, ..}, ..]

        for item in items:
            product_save_task.delay(item)

        return len(list(filter(lambda i: i.get("status") == Product.STATUS_CHOICE_DONE, items)))
    except ConnectionError:
        self.retry(countdown=10)


@shared_task(bind=True, max_retries=1, time_limit=60 * 1, soft_time_limit=60 * 1)
def product_save_task(self, item):
    """Product save task"""
    logger_name = f"{__name__}-{self.request.id}"
    logger = get_task_logger(logger_name)

    status = item.get("status")

    name_url = str(item.get("name_url", "")[:255]).replace('"', "").replace("'", "")
    attributes = item.get("attributes")

    name_url_cleaned = re.sub(r"(\-\d{4})$", "", name_url)
    color_value = attributes.get("color", "") if attributes else ""
    color_value_cleaned = re.sub(r"[\-\/\s]", "-", color_value.lower())

    lang = detect_language(color_value_cleaned)
    color_value_translated = translit(color_value_cleaned, "ru", reversed=lang)
    if color_value_translated:
        name_url_color = f"{name_url_cleaned}-{color_value_translated}"
    else:
        name_url_color = f"{name_url_cleaned}"

    logger.error("Get product by id")
    product = Product.objects.filter(id=item.get("id")).first()
    logger.error("Get product by id 2")

    if product and status == Product.STATUS_CHOICE_DONE:
        for _ in range(3):
            try:
                logger.error("Start save product")
                product.name_url_color = name_url_color if product.name_url_color is None else product.name_url_color

                # product.name = item.get('name', '')[:255]

                product.manufacturer = item.get("manufacturer", "")[:255]
                product.name_url = name_url
                product.price = item.get("price")
                product.front_picture = item.get("front_picture")
                product.back_picture = item.get("back_picture")
                product.description_text = item.get("description_text")
                product.description_html = item.get("description_html")

                product.description_en_text = item.get("description_en_text")
                product.description_en_html = item.get("description_en_html")
                product.model_name = item.get("model_name")
                product.vendor_code = item.get("vendor_code")

                model_name = item.get("model_name")
                category_name = product.category.category_name

                # title
                product.title = (
                    "{model_name} {category_name} - самые выгодные цены в магазине buykers.ru".format(
                        model_name=model_name, category_name=category_name
                    )
                    if not product.category.template_title
                    else str(product.category.template_title).format(
                        model_name=model_name, category_name=category_name
                    )
                )

                # meta_keywords
                product.meta_keywords = (
                    "{model_name}, {category_name}".format(model_name=model_name, category_name=category_name)
                    if not product.category.template_meta_keywords
                    else str(product.category.template_meta_keywords).format(
                        model_name=model_name, category_name=category_name
                    )
                )

                # meta_description
                meta_description_default = (
                    "Купить {model_name} {category_name} в "
                    "наличии, в москве, в россии, в "
                    "магазине buykers.ru "
                    ""
                    "✔ Доступная низкая цена на распродаже, "
                    "{category_name} для мотоциклиста, "
                    "бесплатная доставка ☎ +74953749470"
                )

                product.meta_description = (
                    str(meta_description_default).format(model_name=model_name, category_name=category_name)
                    if not product.category.template_meta_description
                    else str(product.category.template_meta_description).format(
                        model_name=model_name, category_name=category_name
                    )
                )

                # name
                color_with = "Цвет ({0})".format(color_value) if color_value else ""
                product.name = (
                    "{model_name} {category_name} {color}".format(
                        model_name=model_name, category_name=category_name, color=color_value, color_with=color_with
                    )
                    if not product.category.template_name
                    else str(product.category.template_name).format(
                        model_name=model_name, category_name=category_name, color=color_value, color_with=color_with
                    )
                ).strip()

                product.attributes = attributes if attributes else {}
                product.status = Product.STATUS_CHOICE_DONE
                product.is_active = True
                product.save()
                logger.error("End save product")
                return f"DONE: {product.id}"
            except IntegrityError:
                logger.error("Start IntegrityError")
                logger.error("IntegrityError {0}".format(product.pk))

                Product.objects.filter(name_url_color=product.name_url_color).delete()

                product.name_url_color = None
                product.save()

                logger.error("End IntegrityError")
    elif product and status == Product.STATUS_CHOICE_ERROR:
        product.name = item.get("name", "")[:255]
        product.status = Product.STATUS_CHOICE_ERROR
        product.save()
        return f"ERROR: {product.id}"


@shared_task(bind=True, max_retries=1, time_limit=60 * 1, soft_time_limit=60 * 1)
def reparse_list(self):
    logger_name = f"{__name__}"
    logger = get_task_logger(logger_name)

    update_date = datetime.now() - timedelta(days=5)
    products = Product.objects.filter(updated_at__lte=update_date).values_list("id", flat=True)[:50]
    # logger.error(str(list(products)))
    # print(products)

    products = list(products)
    recycle = True
    slice_count = 10
    product_ids = []

    while recycle:
        # collect product ids
        for i in range(slice_count):
            try:
                product_ids.append(products.pop().pk)
            except IndexError:
                recycle = False
                break

        if not product_ids:
            break

        # run task
        product_task.delay(product_ids=product_ids)
        product_ids = []
