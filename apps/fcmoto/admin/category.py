import csv
import datetime
import math
import zipfile

from decimal import Decimal
from io import StringIO, BytesIO

from celery.bin.purge import purge as celery_purge

from conf import celery_app
from django.conf.urls import url
from django.contrib import admin, messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from .forms.forms import GenerateMetaForm
from .inlines import CategoryInfoInlineAdmin
from ..models import Category, Product
from ..tasks import category_task
from ..tasks.category import category_meta_update_task


def generate_meta(request, id):
    id = int(id.replace('/change', ''))

    try:
        category = Category.objects.get(pk=id)
    except Category.DoesNotExist:
        return HttpResponse('DoesNotExist Category')

    opts = Category._meta

    if request.method == 'POST':
        form = GenerateMetaForm(request.POST)
        if form.is_valid():
            category.template_title = form.cleaned_data['title']
            category.template_meta_keywords = form.cleaned_data['meta_keywords']
            category.template_meta_description = form.cleaned_data['meta_description']
            category.template_name = form.cleaned_data['name']
            category.save()
            category_meta_update_task.delay(category_id=category.id)

            messages.add_message(
                request,
                messages.INFO,
                'Задача на обновление META-тегов и заголовка товаров в категории ' + str(category.name) + ' ' + str(
                    category.pk) + ' создана.'
            )

            return HttpResponseRedirect("../")
    else:
        meta_description_default = 'Купить {model_name} {category_name} в ' \
                                   'наличии, в москве, в россии, в ' \
                                   'магазине buykers.ru ' \
                                   '' \
                                   '✔ Доступная низкая цена на распродаже, ' \
                                   '{category_name} для мотоциклиста, ' \
                                   'бесплатная доставка ☎ +74953749470'

        form = GenerateMetaForm(initial={
            'title': category.template_title if category.template_title is not None else '{model_name} {'
                                                                                         'category_name} - самые '
                                                                                         'выгодные цены в магазине '
                                                                                         'buykers.ru',
            'meta_keywords':
                category.template_meta_keywords if category.template_meta_keywords is not None else '{model_name}, '
                                                                                                    '{category_name}',
            'meta_description':
                category.template_meta_description
                if category.template_meta_description is not None else meta_description_default,
            'name': category.template_name if category.template_name is not None else '{model_name}, '
                                                                                      '{category_name}'
        })

    return render(request, 'admin/generate_meta.html', {
        'form': form,
        'opts': opts,
        'category': category
    })


class CategoryAdmin(admin.ModelAdmin):
    """Category Admin"""

    def parse(self, request, categories):
        """Run category parser"""
        for category in categories:
            category_task.delay(category_id=category.id)

    parse.short_description = 'Начать парсинг'

    def export_as_csv(self, request, categories):
        """Export csv file"""

        output = BytesIO()
        f = zipfile.ZipFile(output, 'w', zipfile.ZIP_DEFLATED)

        for category in categories:
            csv_file = StringIO()
            # response = HttpResponse(content_type='text/csv')
            # response['Content-Disposition'] = "attachment; filename=export_webasyst_{category_id}.csv".format(
            #    category_id=category.id)
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')

            # заголовок
            col_names = [
                'Наименование',
                'Наименование артикула',
                'Валюта',
                'Цена',
                'Доступен для заказа',
                'Зачеркнутая цена',
                'Закупочная цена',
                'В наличии',
                'Основной артикул',
                'В наличии @шоу-рум в Москве',
                'В наличии @склад в Москве (1-2 дня)',
                'В наличии @интернет-магазин',
                'ID товара',
                'Краткое описание',
                'Описание',
                'Наклейка',
                'Статус',
                'Тип товаров',
                'Теги',
                'Заголовок',
                'META Keywords',
                'META Description',
                'Ссылка на витрину',
                'Производитель',
                'Пол',
                'Наличие',
                'Категория',
                'Размер',
                'Цвет',
                'Изображения товаров',
                'Изображения товаров',
                'Изображения товаров',
                'Изображения товаров',
                'Изображения товаров',
                'Изображения товаров',
            ]
            csv_writer.writerow([item.encode('utf8').decode('utf8') for item in col_names])

            # # подзаголовок 1
            # col_names = [
            #     '<Категория>',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '1',
            #     '',
            #     '',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера типа «kategoriya»',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            # ]
            # csv_writer.writerow([item.encode('utf8').decode('utf8') for item in col_names])
            #
            # # подзаголовок 1
            # col_names = [
            #     '!подкатегория1',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '1',
            #     '',
            #     '',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера типа «kategoriya1»',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            # ]
            # csv_writer.writerow([item.encode('utf8').decode('utf8') for item in col_names])
            #
            # # подзаголовок 2
            # col_names = [
            #     '!подкатегория2',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '1',
            #     '',
            #     '',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера типа «kategoriya2»',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            # ]
            # csv_writer.writerow([item.encode('utf8').decode('utf8') for item in col_names])
            #
            # # подзаголовок 3
            # col_names = [
            #     '!подкатегория3',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '1',
            #     '',
            #     '',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера',
            #     'текст пользователя парсера типа «kategoriya3»',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            #     '',
            # ]
            # csv_writer.writerow([item.encode('utf8').decode('utf8') for item in col_names])

            products = category.product_set \
                .exclude(name_url_color__isnull=True).exclude(name_url_color__exact='') \
                .filter(status=Product.STATUS_CHOICE_DONE) \
                .distinct('name_url_color')

            bage_html = category.bage_html if category.bage_html is not None else '<div class="badge" ' \
                                                                                  'style="background-color: ' \
                                                                                  '#ff8c2b;">' \
                                                                                  '<span>интернет-магазин</span></div> '

            category_margin = category.margin if category.margin else 0
            category_delivery = category.delivery if category.delivery else 0

            # category info
            category_info_rows = category.categoryinfo_set.order_by('order')

            for category_info in category_info_rows:
                item = [
                    category_info.name,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    category_info.category_webasyst_id,
                    '',
                    category_info.description if category_info.description else '',  # описание
                    '',
                    '1',
                    '',
                    '',
                    category_info.title,
                    category_info.meta_keywords,
                    category_info.meta_description,
                    category_info.link,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ]
                csv_writer.writerow(item)

            for product in products:
                counter = 1
                product_list = []
                all_sizes = []
                all_available = 0
                is_main_article = True

                product_id = ''  # '1{id:011d}'.format(id=product.id)
                name = product.name
                name_url_color = product.name_url_color
                manufacturer = product.manufacturer
                front_picture = product.front_picture
                back_picture = product.back_picture
                if not product.price:
                    continue
                purchase_price = math.ceil(product.price)
                purchase_price_decimal = product.price
                euro_price = product.euro_price()
                euro_rate = product.euro_rate()

                threshold = 200
                charge = Decimal(1.15)

                if euro_price >= threshold:
                    extra_price = purchase_price_decimal - euro_rate * threshold
                    purchase_price_decimal = euro_rate * threshold + extra_price * charge

                online_price = math.ceil(
                    purchase_price_decimal + purchase_price_decimal * category_margin / 100 + category_delivery)

                description_en_html = product.description_en_html

                sizes = product.attributes.get('sizes', [])
                color_value = product.attributes.get('color', '')
                gender_value = product.category.gender if product.category else ''
                in_stock_value = product.category.in_stock if product.category else ''
                keywords = category.keywords if category.keywords is not None else (", ".join(name.split(' ')))

                title = product.title
                meta_keywords = product.meta_keywords
                meta_description = product.meta_description

                for size in sizes:
                    size_value = size.get('size', '').strip()
                    all_sizes.append(size_value)

                    available_value = 10 if size.get('available', False) and product.is_active else 0

                    all_available += available_value

                    if available_value and is_main_article:
                        main_article = str(1)
                        is_main_article = False
                    else:
                        main_article = ''

                    if counter == 1:
                        item = [
                            name,
                            '{size}, {colors}'.format(size=size_value, colors=color_value).strip(', '),
                            'RUB',
                            online_price,
                            str(available_value),
                            str(0),
                            purchase_price,
                            str(available_value),
                            main_article,
                            str(0),
                            str(0),
                            str(available_value),
                            product_id,
                            'Купить {}'.format(name),
                            description_en_html,
                            bage_html,
                            '1',
                            'Одежда',
                            '{keywords}'.format(keywords=keywords).replace('<{}>', ''),
                            title,
                            meta_keywords,
                            meta_description,
                            name_url_color,
                            '',
                            '',
                            '',
                            '',
                            str(size_value),
                            str(color_value),
                            front_picture,
                            back_picture,
                        ]
                        product_list.append(item)
                    else:
                        item = [
                            name,
                            '{size}, {colors}'.format(size=size_value, colors=color_value).strip(', '),
                            'RUB',
                            online_price,
                            str(available_value),
                            str(0),
                            purchase_price,
                            str(available_value),
                            main_article,
                            str(0),
                            str(0),
                            str(available_value),
                            product_id,
                            '',
                            '',
                            bage_html,
                            '1',
                            'Одежда',
                            '',
                            '',
                            '',
                            '',
                            name_url_color,
                            '',
                            '',
                            '',
                            '',
                            str(size_value),
                            str(color_value),
                            '',
                            '',
                        ]
                        product_list.append(item)
                    counter += 1

                all_available = all_available if product.is_active else 0

                # title
                all_size = ",".join(sorted(all_sizes))

                main_item = [
                    name,
                    '',
                    'RUB',
                    online_price,
                    str(1 if all_available else 0),
                    str(0),
                    purchase_price,
                    str(all_available),
                    '',
                    '',
                    '',
                    '',
                    product_id,
                    'Купить {}'.format(name),
                    description_en_html,
                    bage_html,
                    '1',
                    'Одежда',
                    '{keywords}'.format(keywords=keywords).replace('<{}>', ''),
                    title,
                    meta_keywords,
                    meta_description,
                    name_url_color,
                    manufacturer,
                    gender_value,
                    in_stock_value,
                    category.category_name,
                    '<{{{all_size}}}>'.format(all_size=all_size).replace('<{}>', ''),
                    '<{{{color}}}>'.format(color=color_value).replace('<{}>', ''),
                    front_picture,
                    back_picture,
                ]
                product_list.insert(0, main_item)
                [csv_writer.writerow(item) for item in product_list]
            # response['Content-Disposition'] = "attachment; filename=export_webasyst_{category_id}.csv".format(
            #    category_id=category.id)
            # csv_file.seek(0)
            f.writestr(
                "export_webasyst_{category_id}.csv".format(category_id=category.id),
                csv_file.getvalue()
            )
            # return response

        # f.writestr('first.csv', '<the content of first.csv>')
        # f.writestr('second.csv', '<the content of second.csv>')
        # f.writestr('third.csv', '<the content of third.csv>')
        f.close()
        # Build your response
        response = HttpResponse(output.getvalue())
        # response = HttpResponse(output.getvalue(), mimetype='application/zip')
        response['Content-Disposition'] = 'attachment; filename="csv_export_{0}.zip"'.format(
            datetime.datetime.now().strftime('%d-%m-%Y-%H-%M')
        )

        return response

    export_as_csv.short_description = 'Выгрузить в csv'

    def purge(self, request):
        """Purge tasks"""
        celery_purge(app=celery_app).run(force=True, queues='fcmoto_category,fcmoto_page,fcmoto_product')
        return HttpResponseRedirect("../")

    buttons = [
        {
            'url': 'generate_meta',
            'textname': u'Сгенерировать новые мета-теги для категории?',
            'func': generate_meta,
            'confirm': u'Вы уверены, что хотите сгенерировать новые мета-теги?'
        },
    ]

    def change_view(self, request, object_id, form_url='', extra_context={}):
        if not extra_context:
            extra_context = {}
        extra_context['buttons'] = self.buttons
        return super(CategoryAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def get_urls(self):
        """Get url"""
        urls = super(CategoryAdmin, self).get_urls()
        my_urls = list((url(r'^(.+)/%(url)s/$' % b, self.admin_site.admin_view(b['func'])) for b in self.buttons))
        custom_urls = list((
            url('purge/', self.purge),

        ))
        return custom_urls + my_urls + urls

    list_display = ('id', 'name', 'status', 'updated_at',)
    list_filter = ('status',)
    search_fields = ('name',)
    list_per_page = 20
    readonly_fields = (
        'created_at', 'updated_at', 'template_title', 'template_meta_keywords', 'template_meta_description')
    actions = (parse, export_as_csv)
    inlines = [CategoryInfoInlineAdmin, ]
    change_list_template = 'entities/product_changelist.html'
    change_form_template = 'category_change_form.html'
