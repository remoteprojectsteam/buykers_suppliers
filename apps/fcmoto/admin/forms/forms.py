from django import forms


class GenerateMetaForm(forms.Form):
    title = forms.CharField(max_length=100, label='Заголовок', widget=forms.Textarea())

    meta_keywords = forms.CharField(label='META Keywords', max_length=255, widget=forms.Textarea())
    meta_description = forms.CharField(label='META Description', widget=forms.Textarea())
    name = forms.CharField(label='Шаблон наименования', widget=forms.Textarea())
